import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-server-elemrnt',
  templateUrl: './server-elemrnt.component.html',
  styleUrls: ['./server-elemrnt.component.css']
})
export class ServerElemrntComponent implements OnInit {
   @Input ('srvele') element: { type: string, name:string, content:string } 
   // 'srvele' is called alies(parameter) mean a name to pass this object where in (data-binding.component) is catch by this name 
  constructor() { }

  ngOnInit(): void {
  }

}
