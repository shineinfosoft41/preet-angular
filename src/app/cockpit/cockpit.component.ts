import { Component, OnInit,EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-cockpit',
  templateUrl: './cockpit.component.html',
  styleUrls: ['./cockpit.component.css']
})
export class CockpitComponent implements OnInit {
  @Output() serverCreated = new EventEmitter<{serverName:string, serverContent: string }>();
  //An event emitter is a pattern that listens to a named event, fires a callback, then emits that event with a value.
  @Output() blueprintCreated = new EventEmitter<{serverName:string, serverContent: string }>();; 
  newServerName ='';
  newServerContent='';
  constructor() { } 

  ngOnInit(): void {
  }

  onAddServer(){
    this.serverCreated.emit({
      serverName: this.newServerName, 
      serverContent: this.newServerContent
    });
}

onAddBlueprint(){
  this.blueprintCreated .emit({
    serverName: this.newServerName,
     serverContent: this.newServerContent
    });
}

}
