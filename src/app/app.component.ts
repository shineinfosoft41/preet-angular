import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  servers = [];

  onAddServer(){
    this.servers.push('Another Server');
  }

  OnRemoveSever(id : number){
    //const position = id;
    this.servers.splice(id+1, 1);
  } 
  
}
