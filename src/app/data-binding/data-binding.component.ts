import { style } from "@angular/animations";
import { Component } from "@angular/core";

@Component({
    selector : "app-databinding",
    templateUrl : "./data-binding.component.html",
    styleUrls : ['./data-binding.component.css']
})
export class DataBinding{
    serverElements =[{ type: 'server', name:'Testserver', content:'Just a Test !'  }];
   
    onServerAdded( serverData:{serverName:string, serverContent: string } ){
        this.serverElements.push({
            type:'server',
            name: serverData.serverName,
            content : serverData.serverContent
        });
    }
    
    onBlueprintAdded( blueprintData:{serverName:string, serverContent: string } ){
        this.serverElements.push({
            type:'blueprint',
            name:  blueprintData.serverName,
            content: blueprintData.serverContent
        });
    }

   
}
