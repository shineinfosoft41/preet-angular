import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { DataBinding } from './data-binding/data-binding.component';
import { CockpitComponent } from './cockpit/cockpit.component';
import { ServerElemrntComponent } from './server-elemrnt/server-elemrnt.component';

@NgModule({
  declarations: [
    AppComponent,
    DataBinding,
    CockpitComponent,
    ServerElemrntComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
   
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
